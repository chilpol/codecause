<?php

function getAllUsers() {
  return queryDatabase("SELECT * FROM users ORDER BY id DESC");
}

function createUser($email, $password) {
  // validate email uniqueness
  $emails = queryDatabase("SELECT * FROM users WHERE email = ?", array($email));

  if (count($emails)) { 
    createResponse("error", "Email already taken.");
  }

  $password = password_hash($password, PASSWORD_DEFAULT);

  return queryDatabase("INSERT INTO users (email, password) VALUES (?, ?)", array($email, $password));
}

function updateUser($id, $name, $password) {
  if ($password) {
    $password = password_hash($password, PASSWORD_DEFAULT);

    return queryDatabase("UPDATE users SET email = ?, password = ? WHERE id = ?", array($name, $password, $id));
  } else {
    return queryDatabase("UPDATE users SET email = ? WHERE id = ?", array($name, $id));
  }  
}

function deleteUser($id) {
  if (userCount() == 1) { 
    createResponse("error", "Last user cannot be deleted.");
  }

  return queryDatabase("DELETE FROM users WHERE id = ?", array($id));
}

function userCount() {
  return count(queryDatabase("SELECT * FROM users"));
}

?>